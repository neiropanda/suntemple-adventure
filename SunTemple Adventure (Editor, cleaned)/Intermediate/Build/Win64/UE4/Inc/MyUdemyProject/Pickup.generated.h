// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AMainCharacter;
#ifdef MYUDEMYPROJECT_Pickup_generated_h
#error "Pickup.generated.h already included, missing '#pragma once' in Pickup.h"
#endif
#define MYUDEMYPROJECT_Pickup_generated_h

#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_SPARSE_DATA
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_RPC_WRAPPERS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_EVENT_PARMS \
	struct Pickup_eventOnPickupBP_Parms \
	{ \
		AMainCharacter* Target; \
	};


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_CALLBACK_WRAPPERS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPickup(); \
	friend struct Z_Construct_UClass_APickup_Statics; \
public: \
	DECLARE_CLASS(APickup, AItem, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(APickup)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPickup(); \
	friend struct Z_Construct_UClass_APickup_Statics; \
public: \
	DECLARE_CLASS(APickup, AItem, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(APickup)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APickup(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APickup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickup(APickup&&); \
	NO_API APickup(const APickup&); \
public:


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickup(APickup&&); \
	NO_API APickup(const APickup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APickup)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_PRIVATE_PROPERTY_OFFSET
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_12_PROLOG \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_EVENT_PARMS


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_RPC_WRAPPERS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_CALLBACK_WRAPPERS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_INCLASS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_CALLBACK_WRAPPERS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_INCLASS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYUDEMYPROJECT_API UClass* StaticClass<class APickup>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Pickup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
