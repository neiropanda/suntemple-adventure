// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYUDEMYPROJECT_MainCharacter_generated_h
#error "MainCharacter.generated.h already included, missing '#pragma once' in MainCharacter.h"
#endif
#define MYUDEMYPROJECT_MainCharacter_generated_h

#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_SPARSE_DATA
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execLoadGame) \
	{ \
		P_GET_UBOOL(Z_Param_SetPosition); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->LoadGame(Z_Param_SetPosition); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSaveGame) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SaveGame(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDeathEnd) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DeathEnd(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlaySwingSound) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PlaySwingSound(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAttackEnd) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AttackEnd(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncrementHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Amount); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->IncrementHealth(Z_Param_Amount); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncrementCoins) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Amount); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->IncrementCoins(Z_Param_Amount); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execShowPickupLocation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ShowPickupLocation(); \
		P_NATIVE_END; \
	}


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execLoadGame) \
	{ \
		P_GET_UBOOL(Z_Param_SetPosition); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->LoadGame(Z_Param_SetPosition); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSaveGame) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SaveGame(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDeathEnd) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DeathEnd(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlaySwingSound) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PlaySwingSound(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAttackEnd) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AttackEnd(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncrementHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Amount); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->IncrementHealth(Z_Param_Amount); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncrementCoins) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Amount); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->IncrementCoins(Z_Param_Amount); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execShowPickupLocation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ShowPickupLocation(); \
		P_NATIVE_END; \
	}


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainCharacter(); \
	friend struct Z_Construct_UClass_AMainCharacter_Statics; \
public: \
	DECLARE_CLASS(AMainCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(AMainCharacter)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_INCLASS \
private: \
	static void StaticRegisterNativesAMainCharacter(); \
	friend struct Z_Construct_UClass_AMainCharacter_Statics; \
public: \
	DECLARE_CLASS(AMainCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(AMainCharacter)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainCharacter(AMainCharacter&&); \
	NO_API AMainCharacter(const AMainCharacter&); \
public:


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainCharacter(AMainCharacter&&); \
	NO_API AMainCharacter(const AMainCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMainCharacter)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_PRIVATE_PROPERTY_OFFSET
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_31_PROLOG
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_RPC_WRAPPERS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_INCLASS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_INCLASS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h_34_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYUDEMYPROJECT_API UClass* StaticClass<class AMainCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MainCharacter_h


#define FOREACH_ENUM_ESTAMINASTATUS(op) \
	op(EStaminaStatus::ESS_Normal) \
	op(EStaminaStatus::ESS_BelowMinimum) \
	op(EStaminaStatus::ESS_Exhausted) \
	op(EStaminaStatus::ESS_ExhaustedRecovering) 

enum class EStaminaStatus : uint8;
template<> MYUDEMYPROJECT_API UEnum* StaticEnum<EStaminaStatus>();

#define FOREACH_ENUM_EMOVEMENTSTATUS(op) \
	op(EMovementStatus::EMS_Normal) \
	op(EMovementStatus::EMS_Sprinting) \
	op(EMovementStatus::EMS_Dead) 

enum class EMovementStatus : uint8;
template<> MYUDEMYPROJECT_API UEnum* StaticEnum<EMovementStatus>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
