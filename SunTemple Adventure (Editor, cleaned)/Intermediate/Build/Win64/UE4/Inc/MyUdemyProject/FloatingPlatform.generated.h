// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYUDEMYPROJECT_FloatingPlatform_generated_h
#error "FloatingPlatform.generated.h already included, missing '#pragma once' in FloatingPlatform.h"
#endif
#define MYUDEMYPROJECT_FloatingPlatform_generated_h

#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_SPARSE_DATA
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_RPC_WRAPPERS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFloatingPlatform(); \
	friend struct Z_Construct_UClass_AFloatingPlatform_Statics; \
public: \
	DECLARE_CLASS(AFloatingPlatform, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(AFloatingPlatform)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFloatingPlatform(); \
	friend struct Z_Construct_UClass_AFloatingPlatform_Statics; \
public: \
	DECLARE_CLASS(AFloatingPlatform, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(AFloatingPlatform)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFloatingPlatform(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFloatingPlatform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloatingPlatform); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloatingPlatform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloatingPlatform(AFloatingPlatform&&); \
	NO_API AFloatingPlatform(const AFloatingPlatform&); \
public:


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloatingPlatform(AFloatingPlatform&&); \
	NO_API AFloatingPlatform(const AFloatingPlatform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloatingPlatform); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloatingPlatform); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFloatingPlatform)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_PRIVATE_PROPERTY_OFFSET
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_9_PROLOG
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_RPC_WRAPPERS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_INCLASS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_INCLASS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYUDEMYPROJECT_API UClass* StaticClass<class AFloatingPlatform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FloatingPlatform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
