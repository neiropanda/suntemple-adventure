// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYUDEMYPROJECT_MyUdemyProjectGameModeBase_generated_h
#error "MyUdemyProjectGameModeBase.generated.h already included, missing '#pragma once' in MyUdemyProjectGameModeBase.h"
#endif
#define MYUDEMYPROJECT_MyUdemyProjectGameModeBase_generated_h

#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_SPARSE_DATA
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_RPC_WRAPPERS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyUdemyProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AMyUdemyProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyUdemyProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(AMyUdemyProjectGameModeBase)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyUdemyProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AMyUdemyProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyUdemyProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(AMyUdemyProjectGameModeBase)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyUdemyProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyUdemyProjectGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyUdemyProjectGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyUdemyProjectGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyUdemyProjectGameModeBase(AMyUdemyProjectGameModeBase&&); \
	NO_API AMyUdemyProjectGameModeBase(const AMyUdemyProjectGameModeBase&); \
public:


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyUdemyProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyUdemyProjectGameModeBase(AMyUdemyProjectGameModeBase&&); \
	NO_API AMyUdemyProjectGameModeBase(const AMyUdemyProjectGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyUdemyProjectGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyUdemyProjectGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyUdemyProjectGameModeBase)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_12_PROLOG
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_RPC_WRAPPERS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_INCLASS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYUDEMYPROJECT_API UClass* StaticClass<class AMyUdemyProjectGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_MyUdemyProjectGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
