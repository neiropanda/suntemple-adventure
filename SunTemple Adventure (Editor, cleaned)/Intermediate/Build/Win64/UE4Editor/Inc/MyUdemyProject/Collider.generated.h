// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYUDEMYPROJECT_Collider_generated_h
#error "Collider.generated.h already included, missing '#pragma once' in Collider.h"
#endif
#define MYUDEMYPROJECT_Collider_generated_h

#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_SPARSE_DATA
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_RPC_WRAPPERS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACollider(); \
	friend struct Z_Construct_UClass_ACollider_Statics; \
public: \
	DECLARE_CLASS(ACollider, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(ACollider)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACollider(); \
	friend struct Z_Construct_UClass_ACollider_Statics; \
public: \
	DECLARE_CLASS(ACollider, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(ACollider)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACollider(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACollider) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACollider); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACollider); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACollider(ACollider&&); \
	NO_API ACollider(const ACollider&); \
public:


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACollider(ACollider&&); \
	NO_API ACollider(const ACollider&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACollider); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACollider); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACollider)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_PRIVATE_PROPERTY_OFFSET
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_9_PROLOG
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_RPC_WRAPPERS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_INCLASS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_INCLASS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYUDEMYPROJECT_API UClass* StaticClass<class ACollider>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_Collider_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
