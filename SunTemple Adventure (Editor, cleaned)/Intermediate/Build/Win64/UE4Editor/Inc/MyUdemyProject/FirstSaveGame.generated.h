// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYUDEMYPROJECT_FirstSaveGame_generated_h
#error "FirstSaveGame.generated.h already included, missing '#pragma once' in FirstSaveGame.h"
#endif
#define MYUDEMYPROJECT_FirstSaveGame_generated_h

#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCharacterStats_Statics; \
	MYUDEMYPROJECT_API static class UScriptStruct* StaticStruct();


template<> MYUDEMYPROJECT_API UScriptStruct* StaticStruct<struct FCharacterStats>();

#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_SPARSE_DATA
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_RPC_WRAPPERS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_RPC_WRAPPERS_NO_PURE_DECLS
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFirstSaveGame(); \
	friend struct Z_Construct_UClass_UFirstSaveGame_Statics; \
public: \
	DECLARE_CLASS(UFirstSaveGame, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(UFirstSaveGame)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_INCLASS \
private: \
	static void StaticRegisterNativesUFirstSaveGame(); \
	friend struct Z_Construct_UClass_UFirstSaveGame_Statics; \
public: \
	DECLARE_CLASS(UFirstSaveGame, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MyUdemyProject"), NO_API) \
	DECLARE_SERIALIZER(UFirstSaveGame)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFirstSaveGame(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFirstSaveGame) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFirstSaveGame); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFirstSaveGame); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFirstSaveGame(UFirstSaveGame&&); \
	NO_API UFirstSaveGame(const UFirstSaveGame&); \
public:


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFirstSaveGame(UFirstSaveGame&&); \
	NO_API UFirstSaveGame(const UFirstSaveGame&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFirstSaveGame); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFirstSaveGame); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UFirstSaveGame)


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_PRIVATE_PROPERTY_OFFSET
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_47_PROLOG
#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_RPC_WRAPPERS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_INCLASS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_PRIVATE_PROPERTY_OFFSET \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_SPARSE_DATA \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_INCLASS_NO_PURE_DECLS \
	SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h_50_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYUDEMYPROJECT_API UClass* StaticClass<class UFirstSaveGame>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SunTemple_Adventure__Editor__cleaned__Source_MyUdemyProject_FirstSaveGame_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
